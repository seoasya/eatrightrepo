from eatrightrepo.items import EatrightrepoItem
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class MySpider(CrawlSpider):
    name = 'eatright'
    allowed_domains = ['findanrd.eatright.org']
    zip_codes = [91776, 48180, 29105, 5323]
    start_urls = []

    for zip in zip_codes:
        start_urls.append('http://findanrd.eatright.org/listing/search?zipCode=%s' % zip)

    rules = (
        Rule(LinkExtractor(restrict_xpaths='//div[@class="pager"]/a')),
        Rule(LinkExtractor(restrict_xpaths='//div[@class="search-address-list-address"]/a'), callback='parse_item'),
    )

    def parse_item(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        item = EatrightrepoItem()
        item['url'] = response.url
        name_dict = response.xpath('//h1/span[1]/text()').extract()
        item['name'] = name_dict[0].split(', ')[0]
        item['phone'] = response.xpath('//dl[@class="details-right"]//dd/text()').re_first('Phone: (\d.+)')
        item['email'] = response.xpath('//dl[@class="details-right"]')\
            .re_first('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')
        yield item
